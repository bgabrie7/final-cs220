#include "Rook.h"
#include "CreatePiece.h"
#include <cstdlib>
#include <cctype>

typedef std::pair<char, char> c_pair;

bool Rook::legal_move_shape(c_pair start, c_pair end) const {
  return end.first <= 'H' && end.first >= 'A' &&
         end.second <= '8' && end.second >= '1' &&
         start != end &&
    (start.first == end.first || start.second == end.second);
}
