#include "Knight.h"
#include "CreatePiece.h"
#include <cstdlib>
#include <cctype>

typedef std::pair<char, char> c_pair;

bool Knight::legal_move_shape(c_pair start, c_pair end) const {
  return end.first <= 'H' && end.first >= 'A' &&
         end.second <= '8' && end.second >= '1' &&
         start != end &&
    ((abs(start.first - end.first) == 1 &&
         abs(start.second - end.second) == 2) ||
        (abs(start.first - end.first) == 2 &&
         abs(start.second - end.second) == 1));
}
