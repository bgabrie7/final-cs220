CC = g++
CFLAGS = -std=c++11 -Wall -Wextra -pedantic

chess: main.o Board.o Chess.o CreatePiece.o Bishop.o King.o Knight.o Pawn.o Queen.o Rook.o
	$(CC) -o chess main.o Board.o Chess.o CreatePiece.o Bishop.o King.o Knight.o Pawn.o Queen.o Rook.o

Board.o: Board.cpp Bishop.h Board.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h Terminal.h
	$(CC) $(CFLAGS) -c Board.cpp

Chess.o: Chess.cpp Board.h Chess.h Piece.h
	$(CC) $(CFLAGS) -c Chess.cpp 

CreatePiece.o: CreatePiece.cpp Bishop.h Board.h Chess.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h
	$(CC) $(CFLAGS) -c CreatePiece.cpp Pawn.cpp Bishop.cpp King.cpp Knight.cpp Queen.cpp Rook.cpp 

Bishop.o: Bishop.h Piece.h 
	$(CC) $(CFLAGS) -c Bishop.cpp 

King.o: King.h Piece.h
	$(CC) $(CFLAGS) -c King.cpp 

Knight.o: Knight.h Piece.h 
	$(CC) $(CFLAGS) -c Knight.cpp 

Pawn.o: Pawn.h Piece.h         
	$(CC) $(CFLAGS) -c Pawn.cpp 

Queen.o: Queen.h Piece.h   
	$(CC) $(CFLAGS) -c Queen.cpp 

Rook.o: Rook.h Piece.h     
	$(CC) $(CFLAGS) -c Rook.cpp 

main.o: main.cpp Bishop.h Board.h Chess.h King.h Knight.h Mystery.h Pawn.h Piece.h CreatePiece.h Queen.h Rook.h
	$(CC) $(CFLAGS) -c main.cpp 

clean:
	rm -f *.o chess
