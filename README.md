# final-cs220
For Miles:

Pretty much all that remains is testing files and :
- Mystery.h (not sure if we even need to fix this)
- Chess::in_stalemate (use methods below to help. Can call in_mate with
  an int parameter to make sure it doesn't also check if the king is currently
  in mate)
- Board::display (depends on terminal.h)

Note: c_pair in a lot of .cpp files is a typedef for std::pair<char, char>
Note 2: methods changed since I first wrote this, I don't feel like updating all
them but new comments should make it easy to read any files

Useful methods to know: (Methods I added that will help with development)
- in Chess:
  - cross_check: sees if a given piece crosses others for a given moves
  - in_check_at: given a certain position for the king, tells whether the king
                 would be in check there. I use it right now for in_check and
                 in_mate. Probably necessary for in_stalemate
  - get_king: returns position of king
  - legal_capture: sees if capture is allowed for the piece and current board.
  - legal_temp: sees if a passed c_pair is legal and on the board. I use it
                in in_mate to see if a king can move to a certain spot on the
                board, so I know if I need to check if it's in check there.
- in Board:
  - remove_piece: removes a piece from the current position of the board.
  - crosses_pieces: see above
  - start_end_pos: see the method description for what it returns. Useful for
                   looping through a move.
  - is_invalid_char: sees if a given char is a legal char (an actual game piece)
