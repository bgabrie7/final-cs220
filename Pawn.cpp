#include "Pawn.h"
#include "CreatePiece.h"
#include <cstdlib>
#include <cctype>

typedef std::pair<char, char> c_pair;

bool Pawn::legal_move_shape(c_pair start, c_pair end) const {
  if (end.first > 'H' || end.first < 'A' ||
      end.second > '8' || end.second < '1' ||
      start == end)
        return false;
  //if column is not the same
  if (start.first != end.first)
    return false;
  else {
    if(is_white()) {
      //two spaces allowed on first move
      if(start.second == '2' && end.second == '4')
        return true;
      //true if it advances one
      return end.second == start.second + 1;
    } else {
      //two spaces allowed for first move
      if(start.second == '7' && end.second == '5')
        return true;
      //true if advances one
      return end.second == start.second - 1;
    }
  }
}

bool Pawn::legal_capture_shape(c_pair start, c_pair end) const {
  if (end.first > 'H' || end.first < 'A' ||
      end.second > '8' || end.second < '1'||
      start == end) {
        return false;
  }
  if (end.first == start.first + 1 ||
      end.first == start.first - 1) {
    if (is_white()) {
      return end.second == start.second + 1;
    } else {
      return end.second == start.second - 1;
    }
  } else {
    return false;
  }
}
