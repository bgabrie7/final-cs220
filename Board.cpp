#include <iostream>
#include <utility>
#include <map>
#include <cctype>
#include <exception>
#include <string>
#include "Board.h"
#include "CreatePiece.h"
#include "Terminal.h"

using std::pair;
using std::make_pair;
using std::ostream;
using std::cout;
using std::endl;
using std::runtime_error;
using std::string;
using std::map;

typedef pair<char, char> c_pair;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
//Default constructor
Board::Board(){}

//destructor that iterates though the entire board, and deletes the piece if the key leads to a piece pointer
Board::~Board(){
  for (std::map<std::pair<char, char>, Piece*>::const_iterator i = occ.begin(); i != occ.end(); ++i){
      delete i->second;
    }
  }

//copy constructor for const functions that need to simulate scenarios
Board::Board (const Board& o) {
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      c_pair position = make_pair(c, r);
      const Piece* p = o(position);
      p ? add_piece(position, p->to_ascii()) : add_piece(position, '-');
    }
  }
}


// default operator for functions to access a pointer to a specific piece at c_pair pos
// Returns a const pointer to the piece at a prescribed location if it exists,
// or nullptr if there is nothing there.
const Piece* Board::operator() (std::pair<char, char> pos) const {
  if(occ.find(pos) == occ.end()){
    return NULL;
  }
  return occ.find(pos)->second;
}



// Attempts to add a new piece with the specified designator, at the given location.
// Returns false if:
// -- the designator is invalid,
// -- the specified location is not on the board, or
// -- if the specified location is occupied
bool Board::add_piece(c_pair pos, char piece_designator) {
  //return false if piece already exists, position is off board, or the char
  //isn't any piece's
  const Piece * p = (*this)(pos);
  if (p) {
	return false;
  }
  if (pos.first > 'H' || pos.first < 'A' || pos.second < '1' || pos.second > '8' ||
      is_invalid_char(piece_designator)) {
    return false;
  }
  occ[pos] = create_piece(piece_designator);
  return true;
}


//removes the piece from the map
void Board::remove_piece(c_pair loc) {
  delete occ[loc];
  occ[loc] = nullptr;
}


// Displays the board by printing it to stdout
void Board::display() const {
//  cout << *this;

  bool bright = false;
  //append column headers across the top
  cout << "   ";
  for(char c = 'A'; c <= 'H'; c++) {
    Terminal::color_fg(bright, Terminal::Color::RED);
    cout << ' ' << c << ' ';
  }
  cout << endl << "   " << "------------------------" << endl;
  //append header line below column header line

  for(char r='8'; r >= '1'; r--) {
    Terminal::color_fg(bright, Terminal::Color::RED);
    cout << r << ' ' << '|';
    for( char c='A' ; c<='H' ; c++ ) {
      Terminal::color_bg(Terminal::Color::MAGENTA);
      cout << ' ';
      const Piece* piece = (*this)(make_pair(c,r));
      
      if( piece && piece->is_white()) {
	Terminal::color_fg(bright, Terminal::Color::WHITE);
	cout << piece->to_ascii();
	cout << ' ';
      } else if (piece && !piece->is_white()) {
	Terminal::color_fg(bright, Terminal::Color::BLACK);
	cout << piece->to_ascii();
	cout << ' ';
      } else {
	Terminal::color_fg(bright, Terminal::Color::YELLOW);
	cout << '-';
	cout << ' ';
      }
    }
    Terminal::color_fg(bright, Terminal::Color::DEFAULT_COLOR);
    Terminal::color_bg(Terminal::Color::DEFAULT_COLOR);
    cout << endl;
  }

}



// Returns true if the board has the right number of kings on it
bool Board::has_valid_kings() const {
  bool black_king = false;
  bool white_king = false;
  for (char col = 'A'; col <= 'H'; col++) {
    for (char row = '1'; row <= '8'; row++) {
      const Piece * p = (*this)(make_pair(col, row));
      if (p) {
	switch (p->to_ascii()) {
	case 'K':
	  white_king = true;
	  break;
	case 'k':
	  black_king = true;
	  break;
	default:
	  break;
	}
      }
    }
  }
  return white_king && black_king;
}


//returns true if a move will cross a piece in its path
//king, and knight not affected by this function, since their range is either 1 (which eliminates
//the possibility of crossing pieces), or is not affected by pieces in its way
//if piece is not any of the three, call is_crossing.
bool Board::crosses_pieces(c_pair start, c_pair end, char c) const {
  switch (c) {
    //if knight, pawn, or king could make the move, it will be legal
  case 'n':
  case 'N':
  case 'k':
  case 'K':
    return false;
  default:
    break;
  }
  bool b = is_crossing(start, end, c);
  return b;
}


//for pieces that move diagonally, vertically, and horizontally
//check whether there are other pieces between the desired start and end points
//not including the start and end points. 
bool Board::is_crossing(c_pair start, c_pair end, char c) const {
  //vertical
  if (start.first == end.first) {
    if (start.second < end.second){
      for (char ch = start.second + 1; ch < end.second; ch++) {
        if ((*this)(make_pair(start.first, ch)))
	  return true;
      }
    } else {
      for (char ch = end.second + 1; ch < start.second; ch++) {
        if ((*this)(make_pair(start.first, ch)))
	  return true;
      }
    }
    return false;
    //horizontal
  } else if (start.second == end.second) {
    if (start.second < end.second){
      for (char ch = start.first + 1; ch < end.first; ch++) {
        if ((*this)(make_pair(ch, start.second)))
  	  return true;
      }
    } else {
      for (char ch = end.first + 1; ch < start.first; ch++) {
        if ((*this)(make_pair(ch, start.second)))
  	  return true;
      }
    }
    return false;
  }
  if (c == 'm' || c == 'M'){ //if mystery can't move diagonal, it must move x by x instead
    if (!((*this)(start)->legal_move_shape(make_pair('A', '1'), make_pair('H', '8')))){
      return false;
    }
  }
  //diagonal checks (4 separate directions)
  if (start.first - end.first == start.second - end.second){
	if (start.first > end.first){
		for (int i = 0; end.first+1+i < start.first; i++){
			if ((*this)(make_pair(end.first+1+i, end.second+1+i))){
				return true;
			}
		}
	} else {
		for (int i = 0; start.first+1+i < end.first; i++){
			if ((*this)(make_pair(start.first+1+i, start.second+1+i))){
				return true;
			}
		}
	}
  } else if (start.first - end.first == -(start.second - end.second)){
	if (start.first > end.first){
		for (int i = 0; end.first+1+i < start.first; i++){
			if ((*this)(make_pair(end.first+1+i, end.second-1-i))){
				return true;
			}
		}
	} else {
		for (int i = 0; start.first+1+i < end.first; i++){
			if ((*this)(make_pair(start.first+1+i, start.second-1-i))){
				return true;
			}
		}
	}
  }

  return false;
}


//returns the king of the passed color
c_pair Board::get_king_loc(bool white) const {
  c_pair cur;
  const Piece * p;
  for (char col = 'A'; col <= 'H'; col++) {
    for (char row = '1'; row <= '8'; row++) {
      cur = make_pair(col, row);
      p = (*this)(cur);
      //if the piece exists
      if (p) {
	//if c is correct king, returns location
	char c = p->to_ascii();
	if (white && c == 'K')
	  return cur;
	else if (!white && c == 'k')
	  return cur;
      }
    }
  }
  throw runtime_error("No valid kings"); //TODO: delete?
}


//sees if a given char is legal
bool Board::is_invalid_char(char c) const {
  switch (c) {
  case 'K':
  case 'k':
  case 'Q':
  case 'q':
  case 'B':
  case 'b':
  case 'N':
  case 'n':
  case 'R':
  case 'r':
  case 'P':
  case 'p':
  case 'M':
  case 'm':
    return false;
  default:
    return true;
  }
}


//sees if passed pair<char,char> is on the board
bool Board::is_legal_cp(c_pair cp) const {
  return cp.first >= 'A' && cp.first <= 'H' &&
    cp.second >= '1' && cp.second <= '8';
}


//sets current board to empty pieces
void Board::clear_board() {
  for(char col = 'A'; col <= 'H'; col++) {
    for(char row = '1'; row <= '8'; row++) {
      remove_piece(c_pair(col, row));
    }
  }
}


//called from chess function variant. The function is fed who to check check for, and passes the location of
//that player's king as well as the bool to in_check_at.
bool Board::in_check(bool white) const{
  c_pair king_loc = get_king_loc(white);
  return in_check_at(king_loc, white);
}


//called from in_check. The function checks whether or not if (white)'s king is at king_loc, it will be in check.
//function returns the true if king will be in check, and false otherwise.
bool Board::in_check_at(c_pair king_loc, bool white) const {
  for (char col = 'A'; col <= 'H'; col++) {
    for (char row = '1'; row <= '8'; row++) {
      c_pair cp = c_pair(col, row); //TODO: check for leaks from this
      //if piece exists, is not white, and can attack king, return true
      if (white){
	if ((*this)(cp)){
		if (!((*this)(cp)->is_white()) && legal_capture(cp, king_loc, false))
			return true;
	}
      } else {
	if ((*this)(cp)){
		if ((*this)(cp)->is_white() && legal_capture(cp, king_loc, false)) return true;
	}
      }
    }
  }
  //no pieces could attack king, return false
  return false;
}


//checks given a start and end spot, the start piece could move to the end (empty) position.
bool Board::legal_move(c_pair start, c_pair end, bool print) const {
  if (!(*this)(start) || (*this)(end)) //no error messages here, already checks for these
    return false;
  else if (!(*this)(start)->legal_move_shape(start, end)) {
    if (print) cout << "Illegal move shape for that piece" << endl;
    return false;
  } else if (crosses_pieces(start, end, (*this)(start)->to_ascii())) {
    if (print) cout << "Move crosses pieces" << endl;
    return false;
  }
  return true;
}



//checks given a start and end spot, the start piece could move to the end (nonempty) position by capturing
//the occupying piece.
bool Board::legal_capture(c_pair start, c_pair end, bool print) const {
  if (!(*this)(start) || !(*this)(end)) {//no error messages here, already checks for these
    return false;
  } else if ((*this)(start)->is_white() == (*this)(end)->is_white()) {
    if (print) cout << "You cannot attack your own piece!" << endl;
    return false;
  } else if (!(*this)(start)->legal_capture_shape(start, end)) {
    if (print) cout << "Illegal capture shape for that piece" << endl;
    return false;
  } else if (crosses_pieces(start, end, (*this)(start)->to_ascii())) {
    if (print) cout << "Move crosses pieces" << endl;
    return false;
  }
  return true;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
ostream& operator<<(ostream& os, const Board& board) {
  for(char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++) {
      const Piece* piece = board(pair<char, char>(c, r));
      if (piece) {
	os << piece->to_ascii();
      } else {
	os << '-';
      }
    }
    os << endl;
  }
  return os;
}
