#include "Chess.h"
#include "Board.h"
#include <utility>
#include <cctype>
#include <exception>

using std::string;
using std::pair;
using std::make_pair;
using std::cout;
using std::endl;

typedef pair<char, char> c_pair;

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
// This default constructor initializes a board with the standard
// piece positions, and sets the state to white's turn
Chess::Chess() : is_white_turn(true) {
  // Add the pawns
  for (int i = 0; i < 8; i++) {
    board.add_piece(pair<char, char>('A' + i, '1' + 1), 'P');
    board.add_piece(pair<char, char>('A' + i, '1' + 6), 'p');
  }

  // Add the rooks
  board.add_piece(pair<char, char>( 'A'+0 , '1'+0 ) , 'R' );
  board.add_piece(pair<char, char>( 'A'+7 , '1'+0 ) , 'R' );
  board.add_piece(pair<char, char>( 'A'+0 , '1'+7 ) , 'r' );
  board.add_piece(pair<char, char>( 'A'+7 , '1'+7 ) , 'r' );

  // Add the knights
  board.add_piece(pair<char, char>( 'A'+1 , '1'+0 ) , 'N' );
  board.add_piece(pair<char, char>( 'A'+6 , '1'+0 ) , 'N' );
  board.add_piece(pair<char, char>( 'A'+1 , '1'+7 ) , 'n' );
  board.add_piece(pair<char, char>( 'A'+6 , '1'+7 ) , 'n' );

  // Add the bishops
  board.add_piece(pair<char, char>( 'A'+2 , '1'+0 ) , 'B' );
  board.add_piece(pair<char, char>( 'A'+5 , '1'+0 ) , 'B' );
  board.add_piece(pair<char, char>( 'A'+2 , '1'+7 ) , 'b' );
  board.add_piece(pair<char, char>( 'A'+5 , '1'+7 ) , 'b' );

  // Add the kings and queens
  board.add_piece(pair<char, char>( 'A'+3 , '1'+0 ) , 'Q' );
  board.add_piece(pair<char, char>( 'A'+4 , '1'+0 ) , 'K' );
  board.add_piece(pair<char, char>( 'A'+3 , '1'+7 ) , 'q' );
  board.add_piece(pair<char, char>( 'A'+4 , '1'+7 ) , 'k' );
}


// Attemps to make a move. If successful, the move is made and
// the turn is switched white <-> black
bool Chess::make_move(c_pair start, c_pair end) {
  char start_c;
  char c = try_move(start, end);
  //checks if movement causes check for moving player
  if (c != 'f') {
    start_c = board(start)->to_ascii();
    if (move_into_check(start, end)) {
      cout << "Cannot move into check" << endl;
      return false;
    }
  }
  switch (c) {
  case 'c': //capture
    board.remove_piece(end);
    if (!board.add_piece(end, start_c)) {
      throw std::bad_alloc();
    }
    board.remove_piece(start);
    //  check for promotion and change pawn to queen if applicable
    pawn_promotion(end);
    switch_turn();
    return true;
  case 'm': //move
    if (!board.add_piece(end, start_c)) {
      throw std::bad_alloc();
    }
    board.remove_piece(start);
    //	check for promotion and change pawn to queen if applicable
    pawn_promotion(end);
    switch_turn();
    return true;
  case 'f': //false
    return false;
  default:
    throw std::runtime_error("try_move returned impossible char");
  }
  throw std::runtime_error("Reached end of switch"); //TODO: delete?
}


//returns a character that indicates what move will have to be performed. 
//returning c indicates capture (enemy piece at end pos)
//returning m indicates move (no piece at end pos)
//returning f indicates fail (cannot move to end location)
char Chess::try_move(c_pair start, c_pair end) {
  //if either c_pair is not on the board, return 'f'
  if (board(start)->is_white() != turn_white()){
    string msg = (turn_white()) ? "White turn, cannot move black piece." : "Black turn, cannot move white piece.";
    cout << msg << endl;
    return 'f';
  }

  if (!board.is_legal_cp(start)) {
    cout << "Start location not on the board" << endl;
    return 'f';
  }
  if (!board.is_legal_cp(end)) {
    cout << "End location not on the board" << endl;
    return 'f';
  }

  //check piece exists at start, else return 'f'
  if (!board(start)) {
    cout << "No piece at starting location" << endl;
    return 'f';
  }

  //if there is an end piece, try capture
  if (board(end)) {
    //return 'c' if the piece can be captured, 'f' if it cannot
    return legal_capture(start, end, true) ? 'c' : 'f';
  }

  //check if move is legal and piece can be added at end location
  return legal_move(start, end, true) ? 'm' : 'f';
}


//changes a pawn into a queen if it is at the final square
void Chess::pawn_promotion(c_pair cur) {
  char c = board(cur)->to_ascii();
  //if piece is a pawn and on its final square, remove and return Queen
  if (c == 'P' && cur.second == '8') {
    board.remove_piece(cur);
    board.add_piece(cur, 'Q');
  } else if (c == 'p' && cur.second == '1') {
    board.remove_piece(cur);
    board.add_piece(cur, 'q');
  }
}


//checks if a movement causes check
//done by copying const board to a modifiable board copy, executing the move, and checking whether or not
//the color that just moved will be in check because of the move.
bool Chess::move_into_check(c_pair start, c_pair end) const {
  Board new_b = board;
  char start_c = board(start)->to_ascii();
  bool white = board(start)->is_white();
  if (board(end))
    new_b.remove_piece(end);
  new_b.remove_piece(start);
  new_b.add_piece(end, start_c);

  if (new_b.in_check(white)) {
    new_b.clear_board();
    return true;
  }
  new_b.clear_board();
  return false;
}


//Returns true if the designated color is in check
//calls to board equivalent of this function.
bool Chess::in_check(bool white) const{
  return board.in_check(white);
}


//Returns true if the designated player is in mate
bool Chess::in_mate(bool white) const {
	bool a = in_check(white);
	bool b = is_king_trapped(white);
	return a && b;
}


//Returns true if all possible movements/captures of the king will result in check
bool Chess::is_king_trapped(bool white) const {
  c_pair king_loc = board.get_king_loc(white);

  //iterates through every spot on the board and check whether the king can legally move there without being in check.
  //if any grid satisfies this condition, return false as the king is not trapped.
  for (char col = 'A'; col <= 'H'; col++) {
    for (char row = '1'; row <= '8'; row++) {
      if (legal_move(king_loc, c_pair(col, row), false) || legal_capture(king_loc, c_pair(col, row), false)){
	if (!move_into_check(king_loc, c_pair(col, row))) {
	  return false;
	}
      }
    }
  }
  return true;
}


//Returns true if the designated player is not checked, but there are no legal movements for the player to execute
bool Chess::in_stalemate(bool white) const {
  //returns true if the king is not in check and no pieces can move
  return !in_check(white) && is_king_trapped(white) && !can_move(white);
}


//Returns true if a given move is a legal movement
bool Chess::legal_move(c_pair start, c_pair end, bool print) const {
  return board.legal_move(start, end, print);
}


//Returns true if a given move is a legal capture move
bool Chess::legal_capture(c_pair start, c_pair end, bool print) const {
  return board.legal_capture(start, end, print);
}


//Returns true if the king is in check at a given position
bool Chess::in_check_at(c_pair king_loc, bool white) const {
  return board.in_check_at(king_loc, white);
}


//Returns true if a given player has a piece that can move
bool Chess::can_move(bool white) const {
  c_pair cur;
  c_pair temp;
  for (char col = 'A'; col <= 'H'; col++) {
    for (char row = '1'; row <= '8'; row++) {
      cur = make_pair(col, row);
      const Piece * p = board(cur);

      //see if piece exists and is the color we're looking for and not the king
      if (p && p->is_white() == white) {
	char c = p->to_ascii();
	if (c != 'k' && c != 'K') {
	  //check all of the possible positions the piece could move, if it can
      	  //return true
	  for (char col = 'A'; col <= 'H'; col++) {
	    for (char row = '1'; row <= '8'; row++) {
	      temp = make_pair(col, row);
	      if (legal_move(cur, temp, false) || legal_capture(cur, temp, false))
		return true;
	    }
	  }
      	}
      }
    }
  }
  //cannot move anywhere, return false
  return false;
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
// Writes the board out to a stream
std::ostream& operator<< (std::ostream& os, const Chess& chess) {
  // Write the board out and then either the character 'w' or the character 'b',
  // depending on whose turn it is
	return os << chess.get_board() << (chess.turn_white() ? 'w' : 'b');
}


// Reads the board in from a stream
std::istream& operator>> (std::istream& is, Chess& chess) {
  chess.board.clear_board();
  char c;
  //fills board
  for (char row = '8'; row >= '1'; row--) {
    for (char col = 'A'; col <= 'H'; col++) {
      is >> c;
      chess.board.add_piece(c_pair(col, row), c);
    }
  }
  //sets white turn
  is >> c;
  chess.is_white_turn = (c == 'w') ? true : false;
  return is;
}
