#include "Bishop.h"
#include "CreatePiece.h"
#include <iostream> //todo remove
#include <cstdlib>
#include <cctype>

typedef std::pair<char, char> c_pair;

bool Bishop::legal_move_shape(c_pair start, c_pair end) const {
  return end.first <= 'H' && end.first >= 'A' &&
         end.second <= '8' && end.second >= '1' &&
         start != end &&
         abs(start.first - end.first) == abs(start.second - end.second);
}
