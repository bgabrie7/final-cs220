#ifndef CHESS_H
#define CHESS_H

#include <iostream>
#include "Piece.h"
#include "Board.h"

typedef std::pair<char, char> c_pair;

class Chess {

public:
	// This default constructor initializes a board with the standard
	// piece positions, and sets the state to white's turn
	Chess();

	// Returns a constant reference to the board
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	const Board& get_board() const { return board; }

	// Returns true if it is white's turn
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	bool turn_white() const { return is_white_turn; }

	//changes the turn from white to black or vice versa
	void switch_turn() { is_white_turn = is_white_turn ? false : true; }

	// Attemps to make a move. If successful, the move is made and
	// the turn is switched white <-> black
	bool make_move(std::pair<char, char> start, std::pair<char, char> end);

	//returns a character that indicates what move will have to be performed. 
	char try_move(std::pair<char,char> start, std::pair<char,char> end);

	//changes a pawn into a queen if it is at the final square
	void pawn_promotion(c_pair cur);

	//checks if a movement causes check
	bool move_into_check(c_pair start, c_pair end) const;

	//Returns true if the designated player is in check
	bool in_check(bool white) const;

	//Returns true if the designated player is in mate
	bool in_mate(bool white) const;

	//Returns true if the designated player is not checked, but there are no legal movements for the player to execute
	bool in_stalemate(bool white) const;

	//Returns true if all possible movements/captures of the king will result in check
	bool is_king_trapped(bool white) const;

	//Returns true if a given move is a legal movement
	bool legal_move(std::pair<char, char> start, std::pair<char, char> end, bool print) const;

	//Returns true if a given move is a legal capture move
	bool legal_capture(std::pair<char, char> start, std::pair<char, char> end, bool print) const;

	//Returns true if the king is in check at a given position
	bool in_check_at(std::pair<char, char> king_loc, bool white) const;

	//Returns true if a given player has a piece that can move
	bool can_move(bool white) const;

	// Writes the board out to a stream
	friend std::ostream& operator<< (std::ostream& os, const Chess& chess);

	// Reads the board in from a stream
	friend std::istream& operator>> (std::istream& is, Chess& chess);

private:
	// The board
	Board board;

	// Is it white's turn?
	bool is_white_turn;

};

#endif // CHESS_H
