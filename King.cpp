#include "King.h"
#include "CreatePiece.h"
#include <cstdlib>
#include <cctype>

typedef std::pair<char, char> c_pair;

bool King::legal_move_shape(c_pair start, c_pair end) const {
  if (end.first > 'H' || end.first < 'A' ||
      end.second > '8' || end.second < '1'||
      start == end) {
        return false;
  }
  if (start.first == end.first) {
    return abs(start.second - end.second) == 1;
  } else if (start.second == end.second) {
    return abs(start.first - end.first) == 1;
  }
  return abs(start.first - end.first) == 1 &&
         abs(start.second - end.second) == 1;
}
